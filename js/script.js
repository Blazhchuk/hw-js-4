"use strict"

/*

1. Цикл - це стурктура яка дозволяє виконувати певний блок коду або інструкцій повторно поки умова є істиною

2. Є такі види циклів: "for", "while", "do-while"

3. Цикл do-while на відміну від while при хибній умові виконається один раз, тому що він спочатку виконує код а потім перевірє умову. while не буде виконувати а одразу завершиться

*/

// ex 1

let num1 = NaN;
let num2 = NaN;

while (isNaN(num1)) {
    num1 = +prompt('Введіть перше число: ');
}

while (isNaN(num2)) {
    num2 = +prompt('Введіть друге число: ');
}

let min = Math.min(num1, num2);
let max = Math.max(num1, num2);


for (let i = min; i <= max; i++) {
    if (Number.isInteger(i)) {
        console.log(i);
    }
}

// ex 2

let num = NaN;

while (isNaN(num) || num % 2 !== 0) {
    num = +prompt("Введіть парне число:");
}

console.log(`Ви ввели парне число: ${num}`);
